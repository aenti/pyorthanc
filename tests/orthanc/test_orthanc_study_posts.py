# coding: utf-8
# author: gabriel couture
import os
import unittest
import zipfile

import requests

from pyorthanc import Orthanc
from tests.integration import setup_server
from tests.data import a_patient, a_study


class TestOrthancPatientPosts(unittest.TestCase):

    def setUp(self) -> None:
        self.orthanc = Orthanc(setup_server.ORTHANC_URL)
        self.orthanc.setup_credentials('orthanc', 'orthanc') 


    def given_patient_in_orthanc_server(self):
        setup_server.setup_data()

    def test_givenOrthancWithAPatient_whenAnonymizeAStudyFromFile_thenResultIsIdentifiersOfNewAnonymousStudyAndANewAnonymousStudyIsCreated(self):
        self.given_patient_in_orthanc_server()

        result = self.orthanc.anonymize_study_from_file(a_study.IDENTIFIER, 'tests/integration/orthanc/anon.json')

        self.assertIsInstance(result, dict)
        self.assertIn('ID', result.keys())
        self.assertIn('Path', result.keys())
        self.assertIn('PatientID', result.keys())
        self.assertIn(result['ID'], self.orthanc.get_studies())
        self.assertEqual(
            '20000101',
            self.orthanc.get_study_information(result['ID'])['MainDicomTags']['StudyDate']
        )

    def test_givenOrthancWithAPatient_whenModifyAStudySeriesFromFile_thenANewSeriesIsCreatedWithModifiedTags(self):
        setup_server.clear_data()
        self.given_patient_in_orthanc_server()

        results = self.orthanc.modify_study_series_from_file(a_study.IDENTIFIER, 'tests/integration/orthanc/mod.json')
        series_descriptions = [self.orthanc.get_series_information(series_res['ID'])['MainDicomTags']['SeriesDescription'] for series_res in results]
        print(series_descriptions)
        self.assertIn(
            'ModifiedDescription', series_descriptions
        )

    
