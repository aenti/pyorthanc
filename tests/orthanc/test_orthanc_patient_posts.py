# coding: utf-8
# author: gabriel couture
import os
import unittest
import zipfile

from requests import HTTPError

from pyorthanc import Orthanc
from tests import setup_server
from tests.data import a_patient


class TestOrthancPatientPosts(unittest.TestCase):

    def setUp(self) -> None:
        self.orthanc = Orthanc(setup_server.ORTHANC_URL)
        self.orthanc.setup_credentials('orthanc', 'orthanc') 

    def given_patient_in_orthanc_server(self):
        setup_server.clear_data()
        setup_server.setup_data()

    def test_givenOrthancWithAPatient_whenAnonymizeAPatient_thenResultIsIdentifiersOfNewAnonymousPatientAndANewAnonymousPatientIsCreated(self):
        self.given_patient_in_orthanc_server()

        result = self.orthanc.anonymize_patient(a_patient.IDENTIFIER)

        self.assertIsInstance(result, dict)
        self.assertIn('ID', result.keys())
        self.assertIn('Path', result.keys())
        self.assertIn('PatientID', result.keys())
        self.assertIn(result['ID'], self.orthanc.get_patients())
        self.assertRegex(
            self.orthanc.get_patient_information(result['ID'])['MainDicomTags']['PatientName'],
            'Anonymized.*'
        )

    def test_givenOrthancWithoutAPatient_whenAnonymizeAPatient_thenRaiseHTTPError(self):
        self.assertRaises(
            HTTPError,
            lambda: self.orthanc.anonymize_patient(a_patient.IDENTIFIER)
        )

    def test_givenOrthancWithAPatient_whenArchivingAPatient_thenResultIsBytesOfAValidZipFile(self):
        self.given_patient_in_orthanc_server()

        result = self.orthanc.archive_patient(a_patient.IDENTIFIER)

        self.assertIsInstance(result, bytes)
        with open(a_patient.ZIP_FILE_PATH, 'wb') as file_handler:
            file_handler.write(result)

        a_zip_file = zipfile.ZipFile(a_patient.ZIP_FILE_PATH)
        self.assertIsNone(a_zip_file.testzip())
        os.remove(a_patient.ZIP_FILE_PATH)

    def test_givenOrthancWithoutAPatient_whenArchivingAPatient_thenRaiseHTTPError(self):
        self.assertRaises(
            HTTPError,
            lambda: self.orthanc.archive_patient(a_patient.IDENTIFIER)
        )
