# coding: utf-8
# author: gabriel couture
import unittest

from pyorthanc import Orthanc
from tests import setup_server
from tests.data import a_patient


class TestOrthancPatientDeleters(unittest.TestCase):

    def setUp(self) -> None:
        self.orthanc = Orthanc(setup_server.ORTHANC_URL)
        self.orthanc.setup_credentials('orthanc', 'orthanc') 

    def given_patient_in_orthanc_server(self):
        setup_server.setup_data()

    def test_givenOrthancWithPatient_whenDeletingPatientData_thenResultIsTrue(self):
        self.given_patient_in_orthanc_server()

        result = self.orthanc.delete_patient(a_patient.IDENTIFIER)

        self.assertTrue(result)
