# coding: utf-8
# author: Gabriel Couture
import unittest

from pyorthanc import Orthanc
from pyorthanc.util import Study
from tests import setup_server
from tests.data import a_study


class TestStudy(unittest.TestCase):


    def setUp(self) -> None:
        setup_server.setup_data()

        self.study = Study(
            a_study.IDENTIFIER,
            Orthanc(setup_server.ORTHANC_URL)
        )

        self.study.orthanc.setup_credentials('orthanc', 'orthanc')

    def test_givenAStudy_whenGettingParentPatientIdentifier_thenResultIsExpectedParentPatientIdentifier(self):
        result = self.study.get_parent_patient_identifier()

        self.assertEqual(result, a_study.PARENT_PATIENT_IDENTIFIER)

    def test_givenAStudy_whenGettingPatientMainInformation_thenResultIsExpectedPatientMainInformation(self):
        result = self.study.get_patient_information()

        self.assertDictEqual(result, a_study.PATIENT_MAIN_INFORMATION)

    def test_givenAStudy_whenGettingStudyDate_thenResultIsExpectedDate(self):
        result = self.study.get_date()

        self.assertEqual(result, a_study.DATE)

    def test_givenAStudy_whenGettingStudyID_thenResultIsExpectedID(self):
        result = self.study.get_id()

        self.assertEqual(result, a_study.ID)

    def test_givenAStudy_whenGettingStudyInstanceUID_thenResultIsExpectedUID(self):
        result = self.study.get_uid()

        self.assertEqual(result, a_study.INFORMATION['MainDicomTags']['StudyInstanceUID'])

    def test_givenAStudy_whenGettingReferringPhysicianName_thenResultIsExpectedReferringPhysicianName(self):
        result = self.study.get_referring_physician_name()

        self.assertEqual(result, a_study.REFERRING_PHYSICIAN_NAME)

    def test_givenAStudy_whenBuildingSeries_thenStudyHasSeries(self):
        setup_server.clear_data()
        self.setUp()
        self.study.build_series()

        self.assertEqual(
            len(self.study.get_series()),
            len(a_study.SERIES)
        )

    def test_givenAStudyWithEmptySeries_whenTrimStudy_thenEmptySeriesGetsDeleted(self):
        self.study.build_series()  # When getting building, series do not get built by default
        expected_number_of_series = 0

        self.study.trim()

        self.assertEqual(
            expected_number_of_series,
            len(self.study.get_series())
        )
