# coding: utf-8
# author: gabriel couture
import os
import time
import shutil
import subprocess

import requests

ORTHANC_URL = 'http://localhost:8042'


def setup_data() -> None:
    """Load test dicom files to the test Orthanc server instance
    """
    headers = {'content-type': 'application/dicom'}

    list_of_dicom_file_paths = [
        f'./tests/integration/data/dicom_files/{i}'
        for i in os.listdir('./tests/integration/data/dicom_files/')
    ]

    assert len(list_of_dicom_file_paths) == 3

    for file_path in list_of_dicom_file_paths:
        with open(file_path, 'rb') as file_handler:
            data = file_handler.read()

        requests.post(
            f'{ORTHANC_URL}/instances',
            data=data,
            headers=headers,
            auth=('orthanc', 'orthanc')
        )


def clear_data() -> None:
    """Remove all patient data in the test Orthanc instance with API calls
    """
    patient_identifiers = requests.get(f'{ORTHANC_URL}/patients',auth=('orthanc', 'orthanc')).json()

    for patient_identifier in patient_identifiers:
        requests.delete(f'{ORTHANC_URL}/patients/{patient_identifier}',auth=('orthanc', 'orthanc'))
