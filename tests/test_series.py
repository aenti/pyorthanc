# coding: utf-8
# author: Gabriel Couture
import unittest

from pyorthanc import Orthanc
from pyorthanc.util import Series
from tests import setup_server
from tests.data import a_series


class TestSeries(unittest.TestCase):

    def setUp(self) -> None:
        setup_server.setup_data()

        self.series = Series(
            a_series.IDENTIFIER,
            Orthanc(setup_server.ORTHANC_URL)
        )

        self.series.orthanc.setup_credentials('orthanc', 'orthanc')

    def test_givenASeries_whenGettingManufacturer_thenResultIsExpectedManufacturer(self):
        result = self.series.get_manufacturer()

        self.assertEqual(result, a_series.MANUFACTURER)

    def test_givenASeries_whenGettingParentStudyIdentifier_thenResultIsExpectedParentStudyIdentifier(self):
        result = self.series.get_parent_study_identifier()

        self.assertEqual(result, a_series.PARENT_STUDY)

    def test_givenASeries_whenGettingSeriesInstanceUID_thenResultIsExpectedUID(self):
        result = self.series.get_uid()

        self.assertEqual(result, a_series.INFORMATION['MainDicomTags']['SeriesInstanceUID'])

    def test_givenASeries_whenBuildingInstances_thenPatientHasInstances(self):
        self.series.build_instances()

        self.assertEqual(
            len(self.series.get_instances()),
            len(a_series.INSTANCES)
        )
