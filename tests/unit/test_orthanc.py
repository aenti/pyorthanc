# coding: utf-8
from mock import patch, MagicMock
from pyorthanc import Orthanc
import logging
import datetime
import pytz
import pytest

@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_instances_tags_in_simplified_version', 
        return_value={'0000': {}})#{'StudyDate': '20190101'}})
def test_find_patient_age_in_study_without_date(mock_orth_tags, mock_orth_init, caplog):
    orth = Orthanc('URL')
    p_age = orth.find_patient_age_in_study('0000')
    caplog.set_level(logging.INFO)
    log_messages = [tuple[2] for tuple in caplog.record_tuples]
    assert 'StudyDate not found in any series... assuming today as StudyDate' in log_messages
    assert 'PatientAge not found in study. Looking for PatientBirthDate' in log_messages
    assert 'PatientAge and PatientBirthDate not found in study. Returning None' in log_messages
    assert p_age is None

@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_instances_tags_in_simplified_version', 
        return_value={'0000': {'StudyDate': '20190101'}})
def test_find_patient_age_in_study_with_date(mock_orth_tags, mock_orth_init, caplog):
    orth = Orthanc('URL')
    p_age = orth.find_patient_age_in_study('0000')
    caplog.set_level(logging.INFO)
    log_messages = [tuple[2] for tuple in caplog.record_tuples]
    assert 'StudyDate not found in any series... assuming today as StudyDate' not in log_messages
    assert 'PatientAge not found in study. Looking for PatientBirthDate' in log_messages
    assert 'PatientAge and PatientBirthDate not found in study. Returning None' in log_messages
    assert p_age is None

@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_instances_tags_in_simplified_version', 
        return_value={'0000': {'StudyDate': '20190102', 'PatientBirthDate': '20100101'}})
def test_find_patient_age_in_study_with_date_patient_birthdate(mock_orth_tags, mock_orth_init, caplog):
    orth = Orthanc('URL')
    p_age = orth.find_patient_age_in_study('0000')
    caplog.set_level(logging.INFO)
    log_messages = [tuple[2] for tuple in caplog.record_tuples]
    assert 'StudyDate not found in any series... assuming today as StudyDate' not in log_messages
    assert 'PatientAge not found in study. Looking for PatientBirthDate' in log_messages
    assert 'PatientAge and PatientBirthDate not found in study. Returning None' not in log_messages
    assert p_age == '009Y' 

@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_instances_tags_in_simplified_version', 
        return_value={'0000': {'StudyDate': '20190102', 'PatientAge': '014Y'}})
def test_find_patient_age_in_study_with_date_patient_age(mock_orth_tags, mock_orth_init, caplog):
    orth = Orthanc('URL')
    p_age = orth.find_patient_age_in_study('0000')
    caplog.set_level(logging.INFO)
    log_messages = [tuple[2] for tuple in caplog.record_tuples]
    assert 'StudyDate not found in any series... assuming today as StudyDate' not in log_messages
    assert 'PatientAge not found in study. Looking for PatientBirthDate' not in log_messages
    assert 'PatientAge and PatientBirthDate not found in study. Returning None' not in log_messages
    assert p_age == '014Y' 

@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_information', 
        return_value={"MainDicomTags" : {"StudyDate" : "20200907", "StudyTime" : "093606.816000"}})
@patch('pyorthanc.Orthanc.get_study_instances_tags_in_simplified_version', 
        return_value={'0000': {'StudyDate': '20200907', "StudyTime" : "093606.816000"}})
def test_parse_study_date(mock_orth_tags, mock_orth_init, caplog):
    orth = Orthanc('URL')
    (st_datetime, st_date, st_time) = orth.parse_study_date('0000')
    assert st_datetime == datetime.datetime(2020, 9, 7, 9, 36, 6)

@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_instances_tags_in_simplified_version', 
        return_value={'0000': {'StudyDate': '20190102', 'PatientAge': ''}})
def test_find_patient_age_in_study_with_date_empty_patient_age(mock_orth_tags, mock_orth_init, caplog):
    orth = Orthanc('URL')
    p_age = orth.find_patient_age_in_study('0000')
    caplog.set_level(logging.INFO)
    log_messages = [tuple[2] for tuple in caplog.record_tuples]
    assert 'StudyDate not found in any series... assuming today as StudyDate' not in log_messages
    assert 'PatientAge not found in study. Looking for PatientBirthDate' in log_messages
    assert 'PatientAge and PatientBirthDate not found in study. Returning None' in log_messages
    assert p_age is None

@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_instances_tags_in_simplified_version', 
        return_value={'0000': {'StudyDate': '', 'PatientAge': ''}})
def test_find_patient_age_in_study_with_date_empty_study_date(mock_orth_tags, mock_orth_init, caplog):
    orth = Orthanc('URL')
    p_age = orth.find_patient_age_in_study('0000')
    caplog.set_level(logging.INFO)
    log_messages = [tuple[2] for tuple in caplog.record_tuples]
    assert 'StudyDate not found in any series... assuming today as StudyDate' in log_messages
    assert 'PatientAge not found in study. Looking for PatientBirthDate' in log_messages
    assert 'PatientAge and PatientBirthDate not found in study. Returning None' in log_messages
    assert p_age is None


@patch('os.path.exists')
@patch('pyorthanc.Orthanc.__init__', return_value=None)
@patch('pyorthanc.Orthanc.get_study_series_information', 
        return_value=[{'ID': '0000','MainDicomTags': {'SeriesDescription': 'test'}}])
@patch('pyorthanc.Orthanc.get_series_instances', 
        return_value=[{'ID': '00025'}])
@patch('pyorthanc.Orthanc.download_instances_in_parallel')
def test_download_series_by_description(
        mock_orth_download, mock_orth_inst, mock_orth_tags, mock_orth_init, mock_os_exists, caplog
):
        orth = Orthanc('URL')
        mock_os_exists.return_value = True
        orth.download_series_by_description('0000', ['test'], 'test_path')
        caplog.set_level(logging.INFO)
        log_messages = [tuple[2] for tuple in caplog.record_tuples]
        assert 'Series already exists in target dir.' in log_messages

        mock_os_exists.return_value = False
        orth.download_series_by_description('0000', ['test'], 'test_path')
        caplog.set_level(logging.INFO)
        log_messages = [tuple[2] for tuple in caplog.record_tuples]
        mock_orth_download.assert_called_once()
    