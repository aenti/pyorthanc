import unittest
from pyorthanc import helpers
from datetime import date, datetime, timedelta

class TestHelpers(unittest.TestCase):

    def test_calculate_age_with_same_month_and_day(self):
        a_date = date(2019,8,25)
        birth_date = date(2000,8,25)
        expected_age = 19
        result = helpers.calculate_age(a_date, birth_date)
        self.assertEqual(result,expected_age)

    def test_calculate_age_with_same_month_and_a_date_day_lesser_than_birth_date_day(self):
        a_date = date(2019,8,2)
        birth_date = date(2000,8,25)
        expected_age = 18
        result = helpers.calculate_age(a_date, birth_date)
        self.assertEqual(result,expected_age)

    def test_calculate_age_with_same_month_and_a_date_day_greater_than_birth_date_day(self):
        a_date = date(2019,8,28)
        birth_date = date(2000,8,25)
        expected_age = 19
        result = helpers.calculate_age(a_date, birth_date)
        self.assertEqual(result,expected_age)

    def test_calculate_age_with_a_date_month_greater_than_birth_date_month(self):
        a_date = date(2019,9,28)
        birth_date = date(2000,8,25)
        expected_age = 19
        result = helpers.calculate_age(a_date, birth_date)
        self.assertEqual(result,expected_age)

    def test_calculate_age_with_a_date_month_lesser_than_birth_date_month(self):
        a_date = date(2019,7,28)
        birth_date = date(2000,8,25)
        expected_age = 18
        result = helpers.calculate_age(a_date, birth_date)
        self.assertEqual(result,expected_age)

    def test_delta_to_patient_age_1day(self):
        delta = timedelta(days=1)
        patient_age_years = helpers.delta_to_patient_age(delta, units='Y')
        patient_age_days = helpers.delta_to_patient_age(delta, units='D')
        assert (patient_age_days == '001D' and patient_age_years == '000Y')
