import time
import hashlib
import concurrent.futures

import multiprocessing
import datetime
from typing import Callable, List
from multiprocessing import Pool
from tqdm import tqdm


def encrypt_string(hash_string: str) -> str:
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature


def parallel_process(function: Callable, array: List, n_jobs: int = None, use_tqdm: bool = True):
    """
        A parallel version of the map function with a progress bar.

        Args:
            array (array-like): An array to iterate over.
            function (function): A python function to apply to the elements of array
            n_jobs (int, default=None): The number of cores to use
        Returns:
            [function(array[0]), function(array[1]), ...]
    """
    if not n_jobs:
        n_jobs = multiprocessing.cpu_count()*5 
    res = []
    with concurrent.futures.ThreadPoolExecutor(n_jobs) as executor:
        # Start the load operations and mark each future with its URL
        if type(array[0]) == tuple:
            future_to_args = {executor.submit(function, *args): args for args in array}
        else:
            future_to_args = {executor.submit(function, arg): arg for arg in array}

        if use_tqdm:
            for future in tqdm(concurrent.futures.as_completed(future_to_args),total=len(array)):
                args = future_to_args[future]
                try:
                    data = future.result()
                except Exception as exc:
                    print('%r generated an exception: %s' % (args, exc))
                else:
                    res.append(data)
        else:
            for future in concurrent.futures.as_completed(future_to_args):
                args = future_to_args[future]
                try:
                    data = future.result()
                except Exception as exc:
                    print('%r generated an exception: %s' % (args, exc))
                else:
                    res.append(data)
    return res

def calculate_age(a_date: datetime.date, birth_date:datetime.date) -> int:
    """
        Calculate age based on a date and birth date.

        Args:
            a_date: A date to compare with birth_date.
            birth_date: birth_date 
        Returns:
            age
    """
    count_this_year = (a_date.month, a_date.day) < (birth_date.month, birth_date.day)
    age = a_date.year - birth_date.year - count_this_year
    return age

def delta_to_patient_age(delta, units):

    CONVERSION_TO_DAYS = {  # conversion to days
        "Y": 365.25,
        "M": 30,
        "W": 7,
        "D": 1,
    }
    age_in_days = delta.days
    age_target_unit = int(age_in_days / CONVERSION_TO_DAYS.get(units))
    patient_age = f'{age_target_unit:03}{units}'
    return patient_age
