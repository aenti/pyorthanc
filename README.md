PyOrthanc
=========
Python library that wrap the Orthanc REST API and facilitate the manipulation of data.

Link to Orthanc web site: https://www.orthanc-server.com/

__Notes__:
**This is not the original PyOrthanc repo. This is a fork by Entelai.**
Please note that this is an early version of the wrapper (version < 1.0),
therefore some methods description (and maybe name) may change because 
they don't describe adequately the behavior of the corresponding Orthanc REST API route.
Also note that this librairy is still under development.
If the description of an `Orthanc` method does not correspond to the planned 
behavior, please do an issue.

However, 'PyOrthanc' contains objects and functions that may be
useful for anyone writing python script to interact with Orthanc.

Also note that tests (```python setup.py test```) might only work
on a linux machine.


Installation
------------

For dev:
```sh
$ cd pyorthanc-repo
$ pip install -e .
```

```sh
$ cd pyorthanc-repo
$ pip install .
```

To install the original PyOrthanc from pip repository
```sh
$ pip install pyorthanc
```

#### Specific version
If you are looking for a specific version, look here: https://gitlab.physmed.chudequebec.ca/gacou54/pyorthanc/tags.

Example of usage
----------------
Be sure that Orthanc is running. The default URL (if running locally) is `http://localhost:8042`.

<<<<<<< HEAD
#### Connect to Orthanc server and basic usage:
=======
#### Getting access to patients, studies, series and instances information:
>>>>>>> 8fea00d3fa0de74c3d1febdbabb7d14716d636fa
```python
from pyorthanc import Orthanc, util

orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

# To get patients identifier and main information
patients_identifiers = orthanc.get_patients()

for patient_identifier in patients_identifiers:
    patient_information = orthanc.get_patient_information(patient_identifier)

    patient_name = patient_information['MainDicomTags']['name']
    ...
    study_identifiers = patient_information['Studies']    

# To get patient's studies identifier and main information
for study_identifier in study_identifiers:
    study_information = orthanc.get_study_information(study_identifier)
    
    study_date = study_information['MainDicomTags']['StudyDate']
    ...
    series_identifiers = study_information['Series']

# To get study's series identifier and main information
for series_identifier in series_identifiers:
    series_information = orthanc.get_series_information(series_identifier)
    
    modality = series_information['MainDicomTags']['Modality']
    ...
    instance_identifiers = series_information['Instances']

# and so on ...
for instance_identifier in instance_identifiers:
    instance_information = orthanc.get_instance_information(instance_identifier)
    ...
```

#### Build a patient tree structure of all patients in Orthanc instance:
Each patient is a tree. Layers in each tree are `Patient` -> `Study` -> `Series` -> `Instance`.
```python
from pyorthanc import Orthanc, build_patient_forest

patient_forest = build_patient_forest(
    Orthanc('http://localhost:8042/')
)    

for patient in patient_forest:
    patient_info = patient.get_main_information()
    patient.get_name()
    patient.get_zip()
    ...
    
    for study in patient.get_studies():
        study.get_date()
        study.get_referring_physician_name()
        ...

        for series in study.get_series():
            ...
```

<<<<<<< HEAD
#### Download series instances in parallel:
=======

#### Upload DICOM files to Orthanc:
>>>>>>> 8fea00d3fa0de74c3d1febdbabb7d14716d636fa
```python
from pyorthanc import Orthanc, util


orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

series_id="02c4fb4f-d5b2ef80-3014358d-269a7031-09de7d08",

orthanc.download_instances_from_series_in_parallel(series_id, '<output>'):


```

<<<<<<< HEAD
#### Upload DICOM files in parallel to Orthanc:
```python
from pyorthanc import Orthanc

orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

orthanc.upload_dcm_files_in_parallel('<path>') # path where dcm files are stored

=======
orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

with open('A_DICOM_INSTANCE_PATH.dcm', 'rb') as file_handler:
    orthanc.post_instances(file_handler.read())
>>>>>>> 8fea00d3fa0de74c3d1febdbabb7d14716d636fa
```

#### Getting list of connected remote modalities:
```python
from pyorthanc import Orthanc

orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

orthanc.get_modalities()
```

#### Query (C-Find) and Retrieve (C-Move) from remote modality:
```python
from pyorthanc import RemoteModality, Orthanc

orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

remote_modality = RemoteModality(orthanc, 'modality')

# Query (C-Find) on modality
data = {'Level': 'Study', 'Query': {'PatientID': '*'}}
query_response = remote_modality.query(data=data)

# Retrieve (C-Move) results of query on a target modality (AET)
remote_modality.move(query_response['QUERY_ID'], 'target_modality')

# 
import json
orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

query = {
    'Level': 'Patient',
    'Query':{}
}
query_id = orthanc.query_on_modality('remote', data=query)
orthanc.move_query_results_to_given_modality(query_identifier=query_id['ID'],
                                                data=json.dumps({
                                                    "TargetAet": 'TARGETAET',
                                                    "Synchronous":False
                                                })
```

#### Anonymize patient and get file:
```python
from pyorthanc import Orthanc

orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

a_patient_identifier = orthanc.get_patients()[0]

orthanc.anonymize_patient(a_patient_identifier)

# result is: (you can retrieve DICOM file from ID)
# {'ID': 'dd41f2f1-24838e1e-f01746fc-9715072f-189eb0a2',
#  'Path': '/patients/dd41f2f1-24838e1e-f01746fc-9715072f-189eb0a2',
#  'PatientID': 'dd41f2f1-24838e1e-f01746fc-9715072f-189eb0a2',
#  'Type': 'Patient'}
```


#### Anonymize study replacing and keeping tags
```python
orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

study_info = orthanc.get_study_information('c4f4ec96-d64fa148-26d96d4c-a11810cb-a446afda')
# Get tags to replace
study_id = study_info['MainDicomTags']['StudyID']
patient_id = study_info['PatientMainDicomTags']['PatientID']
patient_birth_date = datetime.strptime(study_info['PatientMainDicomTags']['PatientBirthDate'], '%Y%m%d').date()
patient_name = study_info['PatientMainDicomTags']['PatientName']
study_date = datetime.strptime(study_info['MainDicomTags']['StudyDate'], '%Y%m%d').date()
age = pyorthanc.helpers.calculate_age(study_date, patient_birth_date)

# Encrypt some tags
encrypted = []
for string in [patient_name, patient_id,  study_id]:
    encrypted.append(helpers.encrypt_string(string))
    
anon_data = {"Replace":{"PatientName": encrypted[0], "PatientAge": '{:0>3}Y'.format(age),
                        "PatientID": encrypted[1], "StudyID": encrypted[2], "StudyDescription": "BRAIN MRI",
                        "StudyDate": "20000101"},
                        "Keep":["StudyDescription", "SeriesDescription", 
                               "PatientSex", "PatientSize", "PatientWeight", 
                               "SmokingStatus", "PregnancyStatus", "InstitutionName", 
                               "StackSequence"], 
                        "KeepPrivateTags":True, "Force": True}

client.anonymize_study('c4f4ec96-d64fa148-26d96d4c-a11810cb-a446afda', anon_data)

# result is: 
#{'ID': '48970665-f8fd38e2-1bffe682-3e9ad8b7-6f1a56d5',
# 'Path': '/studies/48970665-f8fd38e2-1bffe682-3e9ad8b7-6f1a56d5',
# 'PatientID': '65b6a891-6a3575c6-4da3881f-944ef5f1-5dfa09c5',
# 'Type': 'Study'}
```

#### Anonymize study from json file
```python
>>>  cat anon_config.json
{
    "Replace":{
        "StudyDescription": "BRAIN MRI",
        "StudyDate": "20000101"
    },
    "Keep":[
            "SeriesDescription", 
            "PatientSex"
            ], 
    "KeepPrivateTags":true, 
    "Force": true
}

orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed

orthanc = Orthanc('http://localhost:8042')
orthanc.anonymize_from_file('anon_config.json')

# result is: 
#{'ID': '2fad66fa-3f2e6cd4-a58df69c-7fb7d262-e595ff55',
#'Path': '/studies/2fad66fa-3f2e6cd4-a58df69c-7fb7d262-e595ff55',
#'PatientID': '67759569-80455ba5-dd4d826c-4e27da74-a108d9d3',
#'Type': 'Study'}
```

#### Modify study series from json file (WARNING: delete_original=True flag remove original Series)
```python
>>>  cat mod_config.json
    [
        {
            "Field": "SeriesDescription", // Should be accesible in MainDicomTags
            "From": "^.*flair.*$",)
            "To": "FLAIR"
        },
        {
            "Field": "SeriesDescription",
            "From": "^.*t1.*$",
            "To": "T1"
        }
    ]

orthanc = Orthanc('http://localhost:8042')
orthanc.setup_credentials('username', 'password')  # If needed
orthanc.modify_series_from_file('c4f4ec96-d64fa148-26d96d4c-a11810cb-a446afda', 'mod_config.json', delete_original=True)

```